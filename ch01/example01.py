import chainer
import numpy as np
from chainer import cuda, optimizers, serializers, utils, gradient_check
from chainer import Link, Chain, ChainList
import chainer.functions as F
import chainer.links as L